provider "digitalocean" {
  token = var.secrets["digitalocean_token"]
}

resource "digitalocean_ssh_key" "acpatt" {
  name       = "acpatt"
  public_key = var.id_rsa_pub
}
