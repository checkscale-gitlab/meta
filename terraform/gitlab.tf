# Define the provider. In this case we're building in gitlab
provider "gitlab" {
  token = var.secrets["gitlab_token"]
}

// I can't create a user automatically, so I made a user manually. So go ge tthe details for it.
data "gitlab_user" "alerts" {
  username = "acpatt.alerts"
}
// create a group
resource "gitlab_group" "the-junkyard" {
  name             = var.group.name
  path             = var.group.path
  description      = var.group.description
  visibility_level = var.group.visibility_level
}

// Add alerts user to the group
resource "gitlab_group_membership" "junkyard-alerts" {
  group_id     = gitlab_group.the-junkyard.id
  user_id      = data.gitlab_user.alerts.id
  access_level = "guest"
}

// create the group labels
module "group-label-scope" {
  count      = length(local.label-types)
  source     = "./modules/group-label-scope"
  label-type = local.label-types[count.index]
  group-id   = gitlab_group.the-junkyard.id
}

// Create a project for each server listed
resource "gitlab_project" "server-project" {
  for_each                   = local.projects
  name                       = each.value.name
  path                       = each.key
  description                = each.value.description
  namespace_id               = gitlab_group.the-junkyard.id
  visibility_level           = each.value.visibility_level
  container_registry_enabled = each.value.contained
  tags                       = each.value.tags
  default_branch             = "main"
}

resource "gitlab_service_pipelines_email" "email" {
  for_each                     = local.projects
  project                      = gitlab_project.server-project[each.key].id
  recipients                   = [var.emails.alerts]
  notify_only_broken_pipelines = true
  branches_to_be_notified      = "protected"
}

// Every repo should have production protected.
resource "gitlab_branch_protection" "production" {
  for_each           = local.projects
  project            = gitlab_project.server-project[each.key].id
  branch             = "production"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}
// Every repo can have a weekly schedule
resource "gitlab_pipeline_schedule" "weekly" {
  for_each    = local.projects
  project     = gitlab_project.server-project[each.key].id
  description = "Weekly pipeline run to check that everything still works."
  ref         = "main"
  cron        = "0 5 * * 6"
}



// create vault-passes
resource "gitlab_project_variable" "vault_passes" {
  for_each = {
    for key, value in local.projects :
    key => value
    if value.set_vault_password
  }
  environment_scope = "0"
  project           = gitlab_project.server-project[each.key].id
  key               = "VAULT_PASSWORD"
  value             = var.vault_passes[each.key]
  protected         = true
  masked            = true
}




resource "gitlab_deploy_token" "wellington-ansible-token" {
  # This provides a key so that wellington can download hte ansible repo and use it.
  project    = "${var.group.path}/ansible"
  name       = "Wellington's deploy key"
  username   = "wellington"
  scopes     = ["read_repository"]
  expires_at = "2023-03-14T00:00:00Z"
}
