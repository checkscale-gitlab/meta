variable "label-type" {
  description = "label type definition with values"
  type = object({
    key         = string
    colour      = string
    description = string
    values      = list(string)
  })
}

variable "group-id" {
  description = "description of the group to which to add the labels"
  type        = string
}