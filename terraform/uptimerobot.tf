# Define the provider. In this case we're building in uptimerobot
provider "uptimerobot" {
  api_key = var.secrets["uptimerobot_api_key"]
}
data "uptimerobot_account" "account" {}

data "uptimerobot_alert_contact" "default_alert_contact" {
  friendly_name = data.uptimerobot_account.account.email
}

resource "uptimerobot_alert_contact" "alerts" {
  friendly_name = "alerts"
  type          = "email"
  value         = var.emails.alerts
}

resource "uptimerobot_alert_contact" "backup" {
  friendly_name = "backup"
  type          = "email"
  value         = var.secrets["backup_email"]
}

