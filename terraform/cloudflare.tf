provider "cloudflare" {
  email     = var.secrets["backup_email"]
  api_token = var.secrets["cloudflare_api_token"]
}

resource "cloudflare_zone" "primary" {
  zone = "acpatt.com"
  plan = "free"
}

resource "cloudflare_record" "cname" {
  zone_id = cloudflare_zone.primary.id
  name    = "home"
  value   = var.home_ip
  type    = "A"
  proxied = false
}
